<?php

namespace App\Controller;

use App\Form\MailType;
use App\Repository\NewsletterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class MailController extends AbstractController
{
    /**
     * @Route("/mail/{id}", name="mail")
     */
    public function index(NewsletterRepository $newsletterRepository, Request $request, MailerInterface $mailer, int $id): Response
    {
        $newsletter = $newsletterRepository->findBy(array('id' => $id))[0];
        $entries = $newsletter->getEntries()->toArray();

        usort($entries, function ($a, $b){
          $eventTypes = array(
            'VSETH' => 0,
            'EVENTS' => 1,
            'SONSTIGES' => 2,
            'EXTERN' => 3
          );
          $orgTypes = array(
            'VSETH' => 0,
            'Kommission' => 1,
            'Fachverein' => 2,
            'Assoziierte Organisation' => 3,
            'Anerkannte Organisation' => 4,
            'ETH' => 5,
            'Extern' => 6,
          );
          if($eventTypes[$a->getCategory()] == $eventTypes[$b->getCategory()]) {
            if($orgTypes[$a->getStudOrg()] == $orgTypes[$b->getStudOrg()]) {
              if($a->getOrganisation() == $b->getOrganisation()){
                if($a->getTitle() == $b->getTitle()){
                  return 0;    
                } else if (strcmp($a->getTitle(), $b->getTitle() > 0)) {
                  return -1;
                } else {
                  return 1;
                }
              } else if (strcmp($a->getOrganisation(), $b->getOrganisation()) > 0){
                return 1;
              } else {
                return -1;
              }
            } else if ($orgTypes[$a->getStudOrg()] > $orgTypes[$b->getStudOrg()]) {
              return 1;
            } else {
              return -1;
            }
          } else if($eventTypes[$a->getCategory()] > $eventTypes[$b->getCategory()]) {
            return 1;
          } else {
            return -1;
          } 
        });

        $form = $this->createForm(MailType::class);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
          $data = $form->getData();

          $mailaddresses = explode(";", $data['SendTo']);

          foreach($mailaddresses as $address) {
            $email = (new TemplatedEmail())
              ->from('Oliver Klaus (VSETH) <oliver.klaus@vseth.ethz.ch>')
              //->from('Alexander Schoch (VSETH) <alexander.schoch@vseth.ethz.ch>')
              ->to($address)
              ->subject('[VSETH] Newsletter')
              ->htmlTemplate('mail/email.html.twig')
              ->context([
                'newsletter' => $newsletter,
                'entries' => $entries,
              ]);

            $mailer->send($email);
          }

          return $this->redirect($this->generateUrl('main'));
        }

        return $this->render('mail/index.html.twig', [
          'form' => $form->createView(),
        ]);
    }
}
