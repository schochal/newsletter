<?php

namespace App\Controller;

use App\Entity\Newsletter;
use App\Repository\NewsletterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(NewsletterRepository $newsletterRepository): Response
    {
        $newsletters = $newsletterRepository->findAll();

        usort($newsletters, function($a, $b)
        {
            return strcmp($b->getReleaseDate()->format('Y-m-d'), $a->getReleaseDate()->format('Y-m-d'));
        });

        return $this->render('main/index.html.twig', [
            'newsletters' => $newsletters,
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove")
     */
    public function remove(NewsletterRepository $newsletterRepository, int $id) {
      $newsletter = $newsletterRepository->findBy(array('id' => $id))[0];
      $em = $this->getDoctrine()->getManager();

      $em->remove($newsletter);
      $em->flush();

      return $this->redirect($this->generateUrl('main'));
    }
}
