<?php

namespace App\Controller;

use App\Form\SubmitCSVType;
use App\Entity\Entry;
use App\Entity\Newsletter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class NewController extends AbstractController
{
    /**
     * @Route("/new", name="new")
     */
    public function index(Request $request): Response
    {

        $form = $this->createForm(SubmitCSVType::class);

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $file = $request->files->get('submit_csv')['filename'];
            $filename = md5(uniqid()) . '.' . 'csv';
            $file->move($this->getParameter('uploads_dir'), $filename);

            if($file) {
                $em = $this->getDoctrine()->getManager();

                $newsletterAt = $request->request->get('submit_csv')['newsletterAt'];
                $newsletterAtConv = \DateTime::createFromFormat('Y-m-d', $newsletterAt);

                $newsletter = new Newsletter();
                $newsletter->setReleaseDate($newsletterAtConv);

                $em->persist($newsletter);

                if (($handle = fopen($this->getParameter('uploads_dir') . '/' . $filename, "r")) !== FALSE) {
                  while(($row = fgetcsv($handle, 2000, "\t")) !== FALSE) {
                    $entry = new Entry();
                    $entry->setSubmittedAt(\DateTime::createFromFormat('d/m/Y H:i:s', $row[0]));
                    if($row[1] == 'Andere (z.B. ETH, vorher mit kommunikation@vseth.ethz.ch abgesprochen)') {
                      $entry->setStudOrg('Extern');
                    } else if ($row[1] == 'anerkannte Organisation') {
                      $entry->setStudOrg('Anerkannte Organisation');
                    } else if ($row[1] == 'assoziierte Organisation') {
                      $entry->setStudOrg('Assoziierte Organisation');
                    } else {
                      $entry->setStudOrg($row[1]);
                    }
                    $entry->setOrganisation($row[2]);
                    $entry->setSubmitter($row[3]);
                    $entry->setSubmitterEMail($row[4]);
                    $entry->setTitle($row[5]);
                    $startAt = \DateTime::createFromFormat('d/m/Y H:i:s', $row[6] . " " . $row[7]);
                    $endAt = \DateTime::createFromFormat('d/m/Y H:i:s', $row[8] . " " . $row[9]);
                    if(!$startAt) {
                        $entry->setStartAt(null);
                        if(explode(':', $row[5])[0] == 'VSETH') {
                            $entry->setCategory('VSETH');
                            $entry->setCategoryEN('VSETH');
                        }else {
                          $entry->setCategory('SONSTIGES');
                          $entry->setCategoryEN('OTHER');
                        }
                    } else {
                        $entry->setStartAt($startAt);
                        $entry->setCategory('EVENTS');
                        $entry->setCategoryEN('EVENTS');
                    }
                    if(!$endAt)
                        $entry->setEndAt(null);
                    else
                        $entry->setEndAt($endAt);
                    $entry->setPlace($row[10]);
                    $entry->setDescription($this->makeLinks($row[11]));
                    $entry->setTitleEN($row[12]);
                    $entry->setDescriptionEN($this->makeLinks($row[13]));

                    $entry->setNewsletter($newsletter);

                    $entry->setIsChecked(false);

                    $em->persist($entry);
                  }
                } 

                $em->flush();
                return $this->redirect($this->generateUrl('main'));
            }
        }

        return $this->render('new/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    function makeLinks($str) {
      $url_pattern = "/(http|https|ftp|ftps)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?/";
      $str= preg_replace($url_pattern, '<a href="$0">$0</a>', $str);
      return $str;
    }


}
