<?php

namespace App\Form;

use App\Entity\Entry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('Category', ChoiceType::class, [
            'label' => false,
            'choices' => [
              'VSETH' => 'VSETH', 
              'EVENTS' => 'EVENTS', 
              'SONSTIGES' => 'SONSTIGES', 
              'EXTERN' => 'EXTERN', 
            ],
            'required' => true,
          ])
          ->add('Organisation', TextType::class, [
            'label' => false,
          ])
          ->add('StudOrg', ChoiceType::class, [
            'label' => false,
            'choices' => [
              'VSETH' => 'VSETH',
              'Kommission' => 'Kommission',
              'Fachverein' => 'Fachverein',
              'Assoziierte Organisation' => 'Assoziierte Organisation',
              'Anerkannte Organisation' => 'Anerkannte Organisation',
              'ETH' => 'ETH',
              'Extern' => 'Extern',
            ] ,
            'required' => true,
          ])
          ->add('Title', TextType::class, [
            'label' => false,
            //'data' => $entry->getTitle(),
            'required' => true,
          ]) 
          ->add('TitleEN', TextType::class, [
            'label' => false,
            'required' => true,
          ]) 
          ->add('StartAt', DateTimeType::class, [
            'label' => false,
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'with_seconds' => false,
            'required' => false,
          ]) 
          ->add('EndAt', DateTimeType::class, [
            'label' => false,
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'with_seconds' => false,
            'required' => false,
          ]) 
          ->add('Place', TextType::class, [
            'label' => false,
            'required' => false,
          ]) 
          ->add('Description', TextareaType::class, [
            'label' => false,
            'required' => true,
            'attr' => [
              'rows' => 5, 
            ],
          ])
          ->add('DescriptionEN', TextareaType::class, [
            'label' => false,
            'required' => true,
            'attr' => [
              'rows' => 5, 
            ],
          ])
          ->add('Update', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
          'data_class' => Entry::class,
        ]);
    }
}
