# @Copyright    VSETH - Verband der Studierenden an der ETH Zürich.
# @Author       Alexander Schoch <alexander.schoch@vseth.ethz.ch>
 
FROM eu.gcr.io/vseth-public/base:delta
 
# Install needed dependencies:
#  * default-jre-headless: SonarQube is a java application and therefore needs the Java Runtime (good idea to use headless)
#  * wget, unzip: to download & unzipthe SonarQube binary
#  * jq: in the post-start-config.sh script we need to parse a JSON Response from the SonarQube API. For that we use jq.
RUN apt install -y wget curl mariadb-server yarn php php-xml php-intl php-curl php-mysql php-zip git 

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
  php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
  php composer-setup.php && \
  php -r "unlink('composer-setup.php');" && \
  mv composer.phar /usr/local/bin/composer

RUN wget https://get.symfony.com/cli/installer -O - | bash && \
  mv ~/.symfony/bin/symfony /usr/local/bin/symfony

COPY bin /app/bin
COPY composer.json /app/composer.json
COPY composer.lock /app/composer.lock
COPY config /app/config
COPY migrations /app/migrations
COPY public /app/public
COPY src /app/src
COPY symfony.lock /app/symfony.lock
COPY templates /app/templates
COPY .env /app/.env

RUN composer update

RUN symfony console doctrine:schema:update --force

RUN chwon -Rv www-data:www-data /app/*
