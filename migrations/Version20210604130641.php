<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210604130641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entry ADD newsletter_id INT NOT NULL, ADD submitted_at DATETIME NOT NULL, ADD stud_org VARCHAR(255) NOT NULL, ADD organisation VARCHAR(255) NOT NULL, ADD submitter VARCHAR(255) NOT NULL, ADD submitter_email VARCHAR(255) NOT NULL, ADD title VARCHAR(255) NOT NULL, ADD start_at DATETIME DEFAULT NULL, ADD end_at DATETIME DEFAULT NULL, ADD place VARCHAR(255) DEFAULT NULL, ADD description LONGTEXT NOT NULL, ADD title_en VARCHAR(255) NOT NULL, ADD description_en LONGTEXT NOT NULL, ADD category VARCHAR(255) NOT NULL, ADD category_en VARCHAR(255) NOT NULL, ADD is_checked TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE entry ADD CONSTRAINT FK_2B219D7022DB1917 FOREIGN KEY (newsletter_id) REFERENCES newsletter (id)');
        $this->addSql('CREATE INDEX IDX_2B219D7022DB1917 ON entry (newsletter_id)');
        $this->addSql('ALTER TABLE newsletter ADD release_date DATE NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE entry DROP FOREIGN KEY FK_2B219D7022DB1917');
        $this->addSql('DROP INDEX IDX_2B219D7022DB1917 ON entry');
        $this->addSql('ALTER TABLE entry DROP newsletter_id, DROP submitted_at, DROP stud_org, DROP organisation, DROP submitter, DROP submitter_email, DROP title, DROP start_at, DROP end_at, DROP place, DROP description, DROP title_en, DROP description_en, DROP category, DROP category_en, DROP is_checked');
        $this->addSql('ALTER TABLE newsletter DROP release_date');
    }
}
